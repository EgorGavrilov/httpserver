﻿namespace Http
{
    public class Output
    {
        public decimal SumResult { get; set; }
        public int MulResult { get; set; }
        public decimal[] SortedInputs { get; set; }

        public override string ToString()
        {
            var str = $"SumResult={SumResult}\nMulResult = {MulResult} \n SortedInputs = ";

            foreach (var input in SortedInputs)
            {
                str += input + " ";
            }

            return str;
        }
    }
}