﻿namespace Http
{
    public class Input
    {
        public int K { get; set; }
        public decimal[] Sums { get; set; }
        public int[] Muls { get; set; }

        public override string ToString()
        {
            var str = $"K={K}\nMuls = ";

            foreach (var mul in Muls)
            {
                str += mul + " ";
            }

            str += "\nSumD = ";
            foreach (var dec in Sums)
            {
                str += dec + " ";
            }

            return str;
        }
    }
}