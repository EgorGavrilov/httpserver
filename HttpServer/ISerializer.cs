﻿namespace Http
{
    public interface ISerializer
    {
        //todo: сделать метод bool Satisfy(string serializerFormat) и использовать его вместо switch case "json", "xml"
        string Serialize<T>(T obj);
        T Deserialize<T>(string bytes);
        bool Satisfy(string serializerFormat);
    }
}